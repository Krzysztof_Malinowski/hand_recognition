import cv2
import imutils
import numpy as np
from sklearn.metrics import pairwise
import Common as common
from scipy.spatial import distance

class Feature(object):
    """description of class"""

    def __init__(self,file,vectors):
        self.image = cv2.imread(file)
        self.file = file
        self.lower = np.array([0, 48, 80], dtype = "uint8")
        self.upper = np.array([20, 255, 255], dtype = "uint8")
        self.vectors = vectors


    def startLoadTests(self):
        file = self.file
        frame = imutils.resize(self.image, width = 768, height=1024)

        #white hand
        image = self.hand(frame)
        hand = self.segment(image)
        (thresholded, segmented) = hand

        # circle
        (circular_roi, cX, cY,radius) = self.circle(segmented,thresholded)

        #draw circle
        self.drawCircle(image,cX,cY,radius,common.green,1)
        self.drawCircle(image,cX,cY,5,common.green,-1)

        #get poitns
        defects = self.getPoints(segmented)

        for i in range(defects.shape[0]):
            s,e,f,d = defects[i,0]
            start = tuple(segmented[s][0])
            end = tuple(segmented[e][0])
            far = tuple(segmented[f][0])
          
            self.updateTestVectors(file,image,d,cX,cY,end,radius)


        #cv2.drawContours(image, [segmented], -1, (0, 255, 255), 2)
        #cv2.imshow("Image", image)
        #cv2.waitKey(0) & 0xFF== ord("q")


    def segment(self,image,threshold=25):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.bilateralFilter(gray,9,100,100 )

        thresholded = cv2.threshold(gray, threshold, 255, cv2.THRESH_BINARY)[1]
        cnts, hierarchy = cv2.findContours(thresholded.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        if len(cnts) == 0:
            return
        else:
            # based on contour area, get the maximum contour which is the hand
            segmented = max(cnts, key=cv2.contourArea)
            return (thresholded, segmented)


    def circle(self,segmented,thresholded):
        # find the convex hull of the segmented hand region
        chull = cv2.convexHull(segmented)

        # find the most extreme points in the convex hull
        extreme_top    = tuple(chull[chull[:, :, 1].argmin()][0])
        extreme_bottom = tuple(chull[chull[:, :, 1].argmax()][0])
        extreme_left   = tuple(chull[chull[:, :, 0].argmin()][0])
        extreme_right  = tuple(chull[chull[:, :, 0].argmax()][0])

        # find the center of the palm
        cX = (extreme_left[0] + extreme_right[0]) / 2
        cY = (extreme_top[1] + extreme_bottom[1]) / 2

        # find the maximum euclidean distance between the center of the palm
        # and the most extreme points of the convex hull
        distance = pairwise.euclidean_distances([(cX, cY)], Y=[extreme_left, extreme_right, extreme_top, extreme_bottom])[0]
        maximum_distance = distance[distance.argmax()]

        # calculate the radius of the circle with 80% of the max euclidean distance obtained
        radius = int(0.8 * maximum_distance)

        # find the circumference of the circle
        circumference = (2 * np.pi * radius)

        # take out the circular region of interest which has 
        # the palm and the fingers
        circular_roi = np.zeros(thresholded.shape[:2], dtype="uint8")

        return (circular_roi, cX, cY,radius)

    def hand(self,frame):
        converted = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        skinMask = cv2.inRange(converted, self.lower, self.upper)

        # apply a series of erosions and dilations to the mask
        # using an elliptical kernel
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
        skinMask = cv2.erode(skinMask, kernel, iterations = 2)
        skinMask = cv2.dilate(skinMask, kernel, iterations = 2)

        # blur the mask to help remove noise, then apply the
        # mask to the frame
        skinMask = cv2.GaussianBlur(skinMask, (3, 3), 0)
        skin = cv2.bitwise_and(frame, frame, mask = skinMask)
        #color
        skin[np.where((skin != [0,0,0]).all(axis = 2))] = [255,255,255]
        return skin

    def drawCircle(self,image,cX,cY,radius,color,type):
        cv2.circle(image, (int(cX), int(cY)), radius, color, type)

    def drawLine(self,image,cX,cY,end,color,type):
        cv2.line(image,(int(cX), int(cY)),end,color,type)

    def getPoints(self,segmented):
        hull = cv2.convexHull(segmented,returnPoints = False)
        defects = cv2.convexityDefects(segmented,hull)
        return defects

    def updateTestVectors(self,file,image,d,cX,cY,end,radius):
        if file == "C:\\Users\Krzysztof Malinowski\\source\\repos\\OpenCV\\OpenCV\\Images\\Fb1.jpg" or file == "C:\\Users\\Krzysztof Malinowski\\source\\repos\\OpenCV\\OpenCV\\Images\\Fb2.jpg" or file == "C:\\Users\\Krzysztof Malinowski\\source\\repos\\OpenCV\\OpenCV\\Images\\Fb3.jpg":
                if d >2000 and end[0] > 200:
                    self.updateVector(file,image,d,cX,cY,end,radius)
        else:
                if d >2000 and end[0] > 175:
                    self.updateVector(file,image,d,cX,cY,end,radius)

    def updateVector(self,file,image,d,cX,cY,end,radius):
        #draw
        self.drawCircle(image,end[0],end[1],5,common.red,-1)
        self.drawLine(image,cX,cY,end,common.blue,2)

        tempFileName = file.split("\\")
        FileName = tempFileName[8].split(".")
        temp = list()
        temp.append(end)
        temp.append((int(cX), int(cY)))
        temp.append(radius)
        dst = distance.euclidean(end, (int(cX), int(cY)))
        temp.append(dst)
        self.vectors[FileName[0]].append(temp)
