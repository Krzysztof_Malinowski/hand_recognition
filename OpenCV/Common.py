#key codes
quit_key = ord('q')

#strings
preview_window_name = "Preview"
capture_failed = "Capture failed"
template_preview_window_name = "Reference frame"
match_result_window_name = "Match result"
image_path = "Images\Ml1.jpg"
folder = "C:\\Users\Krzysztof Malinowski\\source\\repos\\OpenCV\\OpenCV\\Images\\"

#Colors (BGR)
green = (0,255,0)
red = (0,0,255)
yellow = (0,255,255)
blue = (255,0,0)

#rectangle parameters
rectangle_thickness = 3
