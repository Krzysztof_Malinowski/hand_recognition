from scipy.spatial import distance

class Temporary(object):
    def __init__(self, label, distance):
        self.label = label
        self.distance = distance


class KNN(object):

    """description of class"""

    def __init__(self,k):
            self.k = k

    def predict(self,aKey,aVector,aTestDictionary):

        temp = list()
        keyLists = list()
        for key, value in aTestDictionary.items():
            keyLists.append([key,0])
            for i in range(0,len(value),1):
                dst = distance.euclidean(aVector,value[i])
                temp.append([key,dst])
        temp.sort(key=lambda x: x[1])
        tempLists = temp[:self.k]
        print("Ręka " + aKey + " -> " + tempLists[0][0])

        if aKey == tempLists[0][0]:
            return True
        else:
            return False