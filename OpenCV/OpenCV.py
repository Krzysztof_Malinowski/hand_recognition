# organize imports
import cv2
import os
import Feature as feature
import Common as common
import KNN as knn
import copy
import numpy as np  


vectors =	{
  "Fb1": list(),
  "Hj1": list(),
  "Km1": list(),
  "Ml1": list(),
  "Mm1": list(),
  "Mr1": list(),
  "Pp1": list(),
  "Fb2": list(),
  "Hj2": list(),
  "Km2": list(),
  "Ml2": list(),
  "Mm2": list(),
  "Mr2": list(),
  "Pp2": list(),
  "Fb3": list(),
  "Hj3": list(),
  "Km3": list(),
  "Ml3": list(),
  "Mm3": list(),
  "Mr3": list(),
  "Pp3": list(),
}

new_dict = dict()


#-------------------------------------------------------------------------------
# Main function
#-------------------------------------------------------------------------------
if __name__ == "__main__":

    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(common.folder):
        for file in f:
            if '.jpg' in file:
                files.append(os.path.join(r, file))

    for file in files:
        print(file)
        Feature = feature.Feature(file,vectors)
        Feature.startLoadTests()

    for key, value in vectors.items():
        if not key[:2] in new_dict:
            new_dict[key[:2]] = list()
        #new_dict[key[:2]].append([])
        newVector = []

        for i in range(0,5,1):
            newVector.append(value[i][3])
            #print(value[i][3])
        new_dict[key[:2]].append(newVector)
    countAll = 0
    countAllSuccess = 0
    for key,value in new_dict.items():
        countSuccess = 0

        for i in range(0,3,1):
            tests = copy.deepcopy(new_dict)
            del tests.get(key)[i]
            classification = knn.KNN(1)

            if classification.predict(key,value[i],tests):
                countSuccess += 1
                countAllSuccess += 1
            countAll +=1
        print()
        print(key + " " + str(round(countSuccess/3,3) * 100) + "%")
        print()
    print()
    print("*********************************************")
    print(str(round(countAllSuccess/countAll,3) * 100) + "%")

    cv2.destroyAllWindows()