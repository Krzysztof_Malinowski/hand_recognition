import Common as common
import cv2 as opencv
import imutils
import numpy as np

class CameraCapture:
    #dddd
    is_preview_active = False
    user_rectangle = None

    def __init__(self):
        self.camera_capture = opencv.VideoCapture(0)

        #get fps and calculate delay
        fps = self.camera_capture.get(opencv.CAP_PROP_FPS)
        self.ms_delay = int(1000/fps)
        #init
        opencv.namedWindow(common.preview_window_name,opencv.WINDOW_FREERATIO)
        #set mouse
        opencv.setMouseCallback(common.preview_window_name,self.on_mouse_move)

    def start_preview(self):
        self.is_preview_active = True

        while(self.is_preview_active):
            self.capture_and_display_frame()

    def stop_preview(self):
        self.is_preview_active = False

    def capture_and_display_frame(self):
        #read frame from camera
        (capture_status,self.current_camera_frame) = self.camera_capture.read()

        if capture_status:

            self.draw_user_rectangle()
            #display frame
            opencv.imshow(common.preview_window_name,self.current_camera_frame)

            #check q press
            if(opencv.waitKey(self.ms_delay) == common.quit_key):
                self.stop_preview()
        else:
            print(common.capture_failed)

    def release(self):
        self.stop_preview()
        self.camera_capture.release()
        opencv.destroyAllWindows()

